# 接口规范

## 请求方式

基本原则：读接口用GET，写接口用POST。

最终这个接口用什么请求方式取决于请求接口是否会影响到现有数据。

特殊情况下可以使用POST方式处理读取的接口，比如请求体过大时。

## uri规范

uri规则遵循下面的格式：

`/{module}/{action}.{returnType}?param=value&param1=value1`

其中，大括号括起来的部分分别表示如下含义

### module

表示功能模块，如果功能模块有细分，可以用`/`分隔。

### action

action一般为动词，常用到的罗列如下：

- 创建 create
- 修改 modify
- 详情 detail
- 列表/搜索 search

### returnType

表示数据返回的格式，仅支持json格式即可。

### 示例

- `GET /schedule/search.json?offset=0&limit=20`
- `GET /schedule/detail.json?id=1`
- `POST /feedback/create.json`
- `POST /schedule/submodule/modify.json`

## 返回值规范

返回值必须为一个合法的json字符串，最外层必须包含如下三个字段：

- `error` 表示错误码，int类型
- `message` 表示错误信息，string类型
- `data` 表示返回结果，object类型

`data`字段必须始终保持是 object 类型，如果数据为空请返回空对象。
`error`和`message`字段业务含义需要在文档中明确标识。

### 错误码规范

下面是常见的错误码说明：

1. 0 表示成功。
2. 90001 ~ 90009 表示系统级别的错误,此时表示系统处于故障状态。
3. 30001 ~ 39999 表示业务错误，不同的业务错误建议使用不同的错误码区分。

### 数据字段

1. 小驼峰 `userDetail` 这种
2. 类型必须始终与提供的类型保持一致，如数据为空不可返回null值，需要返回对应类型的空值，或特殊的具有业务含义的值，或不返回。
3. 合理嵌套，层级不要太深。相同含义的字段尽量复用数据结构，方便调用方解析。

### 返回列表数据

由于规定`data`字段必须是object类型， 当返回列表类型的数据时，需要套一个`list`字段。

### 分页特殊说明

请求参数至少有两个选传字段：

1. `offset` int 类型，表示偏移量
2. `limit` int 类型，表示取多少条数据

`data`字段中至少需要包含如下三个字段：

1. `total` int类型，表示当前条件下的数据总量
2. `more` boolean类型，表示当前查询的分页参数下，是否还有更多数据。
3. `list` array类型，表示查询出来的数据列表
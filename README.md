# PHP 开发规范

## 编码规范

开发者需要严格遵守如下编码规范,建议使用IDE自动格式化强制保证。

- [PSR-1](http://www.php-fig.org/psr/psr-1/)
- [PSR-2](http://www.php-fig.org/psr/psr-2/)

## 其他规范

- 字符串语法表示，除非必须，请使用单引号。

- 变量在使用前必须要先进行声明。如以下示例：

```php
<?php

$people = [];
$people['name'] = '张三';

```

- 变量在使用前必须保证类型一致。如以下示例：

```php
<?php

$images = $_POST['images'];

if (!is_array($images)) {
    throw new Exception('images 参数非法');
}

foreach($images as $image) {
    // do something
}

```

- 开发环境需要将 error_reporting 设置为 E_ALL

- 业务逻辑中不可使用 die exit 等，需要退出程序请抛出异常

- 注释建议参考[PSR-5](https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md)

```php
<?php
/**
 * This is a Summary.
 *
 * This is a Description. It may span multiple lines
 * or contain 'code' examples using the _Markdown_ markup
 * language.
 *
 * @see Markdown
 *
 * @param int        $parameter1 A parameter description.
 * @param \Exception $e          Another parameter description.
 *
 * @\Doctrine\Orm\Mapper\Entity()
 *
 * @return string
 */
function test($parameter1, $e)
{
    ...
}
```

- 代码中的变量名，方法名，类名等建议是一个合法的单词
